# 2D Ising Spin System with Arduino

[[_TOC_]]

# Introduction

The Ising Spin System is a very simple model for magnetic materials. Although the 1D and 2D versions of the model is already completely solved analytically, it is simple to implement with a Monte Carlo simulation and results in nice images, which makes it ideal for some Arduino visualizations.

# Theory

In the Ising model, the system is divided into a grid of cells. Each cell has a "spin" which can be up/+1/on, or down/-1/off. In 2D, each cell interacts with the 4 adjacent cells (up and down, left and right) with coupling strength J, and with an external field with coupling strength h. As such, the energy contribution to the total system energy of a single cell $`i`$ with spin $`\sigma_i`$ and neighbors of spin $`\sigma_j`$ is:

$`
E_i = -\sum_j \sigma_i \sigma_j J + -\sigma_i h
`$

## Monte Carlo Simulation

The easiest way to simulate this system is with a Metropolis Monte Carlo approach. Every step, a cell in the system is chosen at random, and the algorithm attempts to flip the spin of the cell (from +1 to -1 or viceversa). This results in the energy of the system being changed, and the new configuration is accepted with probability:

$`
P = \exp(-\frac{(E_{\mathrm{after}}-E_{\mathrm{before}})}{k_{\mathrm{B}} T})
`$

Which means the change always happens if it reduces the energy of the system. Otherwise, there is a probability of changing the system that is related to the energy change (which in turn depends on J and h) and the temperature.

Some simplifications can be done here. We can assume that Boltzmann constant ($`k_B`$) is just 1. Since flipping one spin only affects the energy terms of that cell and its neighbors, the calculation of the energy difference can be simplified (see code).

## Parameters

From the description, there are 3 possible parameters that can be changed in the simulation:
* J: Sets the strength of the neighbor interaction, and whether the system is ferromagnetic (J > 0, spins align), or antiferromagnetic (J < 0, neighboring spins are opposed), or even no interaction.
* h: Bias spins to be on or off when it is different from 0.
* T: Results in a disordered (T >> J) or ordered (T << J) system. Transition happens at around T ~ J.

## Modifications to the setup

The Ising setup can be made more complex by adding more interaction terms into the system. For example, interaction with diagonal neighbors, or weakened interactions with second neighbors, or combinations of ferro and antiferro magnetic interactions, or the presence of more than one species on the system.

# Results

![Initial random setup](Images/P1116024_edited.png "Initial random setup")
System is initialized into a random state.

![Evolved system](Images/P1116025_edited.png "System after a few thousand timesteps")
State of the system after a few thousand timesteps (T=1.0, h=0.0, J=0.5). System has adopted a more ordered configuration with defined spin up and spin down domains. (Pixel size is 2X2).

![High temeprature](Images/P1116026_edited.png "High temperature")
High temperature (T=10.0) randomizes the system again.

![Lowered temperature](Images/P1116027_edited.png "Lowered temeprature")
As temperature is lowered, system reorganizes.

![Antiferromagnetic](Images/P1116028_edited.png "Antiferromagnetic")
Antiferromagnetic configuration (J=-1.0), notice the checkerboard pattern of flipped neighboring cells. Due to temeprature, flipped line frontiers between domains can also be observed.

# Wiring

![Breadboard and Wiring](Images/ising_bb.png "Breadboard and Wiring for the Ising project")

# Bill of Materials

| Component      | Notes    |
| ---                     |  ------  |
| Arduino Nano            | Can be replaced with an UNO. Main difference is the UNO has dedicated SDA and SCL pins for I2C communication, but in the nano these correspond to pins A4 and A5.   |
| Breadboard              |          |
| OLED Display            | 128x64 pixels, I2C protocol, SSD1306 driver compatible |
| RGB LED                 | Indicates status (see extended notes down) |
| 3 Push Buttons          | Sets up modes, increases or decreases values (see notes) |
| 4X 1k$`\Ohm`$ Resistors | 3X pull down resistors for push-buttons, 1X for RGB LED. RGB LED should actually have one resistor per color, but this simplifies wiring and no more than one color should be on at once. |

# Push button controls and display LED

The buttons control the values of the Ising interaction parameters. Originally meant to be a potentiometer for setting the value plus a button for setting the mode (which paramaeter is being changed), but I had no potentiometers at hand. 

How it works now: Program holds a list of possible values for T, h and J. Mode button rotatates (ie, cyclically) between modifying each of the parameters, and RGB LED flashes on with the corresponding color (red for T, green for h, blue for J) to indicate the current mode. Plus and minus buttons go up and down the lists of values, these don't cycle around and thus have two end points for maximum and minimum.

Unfortunately with this setup it is not possible to sweep all possible interaction parameter combinations vs. setting the values with a potentiometer, but it also simplifies the setup. Values have been chosen so many "interresting" combinations are present.

# Notes

* OLED Display: Updating the OLED display is extremely slow. For this reason, multiple MC trial moves are calculated between screen updates. 200-300 steps per display update seems to be a good compromise between calculation speed, and animation FPS.
* Keeping track of system state: To properly keep track of the spins in the system we'd need an 128X96 item array, which even if declared as a boolean would require quite a bit of memory space. Instead we can just use the buffer alread provided by the OLED library, which keeps track of which pixels on the screen are to be turned on or off in the next update.

# Other Features

* Pixel Size: Pixel/cell size can be changed for better visualization. 1X1 and 2X2 seem to be best, larger cells result in a too small system that converges to an all on or all off system (finite size effect).

# To Do

* Turn upper part of OLED into information display (T, interaction params, statistics?) (toggle with button?)
* Set interaction params, and temps with external potentiometers
* Also if simulation stagnates, or after a certain time, reset

# Other possible systems

Given the memory and speed and display limitations, one could think of implementing the following simulations (always in 2D):

* Multiple simulation setups with a Lennard-Jones potential: It would be interesting to see clustering behavior with a Grand Canonical Ensemble simulation for example. 
* Ideal Gas with ballistic trajectories
* Conway's Game of Life: Done to death, but always looks good.
* With a display capable of more than one color: Many multi-elemental simulations

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>

#define TIMING // flag to enable timing code

#define SCREEN_WIDTH_INIT  128 // OLED display width, in pixels
#define SCREEN_HEIGHT_INIT 64 // OLED display height, in pixels

#define PIXEL_WIDTH 5 // Defines the width of the pixels/cells on screen

// Adjust screen sizes to avoid pixels at edges for non multiples
#define SCREEN_WIDTH  SCREEN_WIDTH_INIT-(SCREEN_WIDTH_INIT%PIXEL_WIDTH) // OLED display width, in pixels
#define SCREEN_HEIGHT SCREEN_HEIGHT_INIT-(SCREEN_HEIGHT_INIT%PIXEL_WIDTH) // OLED display height, in pixels

#define OLED_ADDRESS 0x3C // I2C address of my OLED device
// On the nano, SCL and SDA are analog pins 5 and 4, respectively 

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH_INIT, SCREEN_HEIGHT_INIT, &Wire, OLED_RESET);

// Display Pins
#define RLEDPIN 2
#define GLEDPIN 3
#define BLEDPIN 4
#define LEDDELAY 150

// Control Pins
#define MINUSPIN 10
#define PLUSPIN  11
#define MODEPIN  12

#define UNCONNECTED_PIN A0 // For the RNG seed
#define MAX_RAND 1000 // Defines the max edge of the random number generator for generating values between 0 and 1, changes the granularity of the obtained numbers
#define CALC_STEPS 250 // how many steps to calculate before updating the screen; screen update is slow

// moving as many variables out of loop to avoid repeated variable creation (compiler might already optimize this?)
int pixelStatus, pixelX, pixelY;
bool isPixelOn;
float trialSpin, origSpin, spinLeft, spinRight, spinDown, spinUp;
float dE;
float ran;
float metropolis;

// these could be constants or macros maybe?
float Jlist[]  = {-0.5, -0.1, 0.0, 0.1, 0.5, 1.0, 5.0}; // spin coupling
int   Jlength  = sizeof(Jlist)/sizeof(Jlist[0]);
#define Jcounter 4

float hlist[]  = {-0.1, 0.0, 0.1}; // external field coupling
int   hlength  = sizeof(hlist)/sizeof(hlist[0]);
#define hcounter 1

float blist[]  = {1.0/0.01, 1.0/0.1, 1.0/0.5, 1.0/1.0, 1.0/2.0, 1.0/10.0, };
int   blength  = sizeof(blist)/sizeof(blist[0]);// It would be nice to just define a len function, but C is weird and this seems to not be possible due to sizeof shenannigans
#define bcounter 3

// Modes
#define BPOS 0
#define HPOS 1
#define JPOS 2
int modemax     = 2;
int modecounter = 0;
int modepins[]  = {RLEDPIN, GLEDPIN, BLEDPIN};
int modelengths[]  = {blength,  hlength,  Jlength}; 
int modecounters[] = {bcounter, hcounter, Jcounter};

// For button interactions
int buttonState=0, plusState=0, minusState=0, modeState=0;

#ifdef TIMING
    unsigned long execTime;
#endif

float pixel_spin(int x, int y) {
  // returns the spin of a given pixel, translating from a boolean to -1.0 or 1.0
  isPixelOn = display.getPixel(x, y);
  if (isPixelOn){ //if on
    return 1.0;
  }  else {
    return -1.0;
  }
}

void draw_pixel(int x, int y, int mode) { // mode is one of SSD1306_WHITE or _BLACK, which are defined in the 1306 library as macros and 0 or 1
  for (int i=0; i<PIXEL_WIDTH; i++) { // offset in x direction
    for (int j=0; j<PIXEL_WIDTH; j++) { // offset in y direction
      display.drawPixel(x+i, y+j, mode);
    }
  }
}

void setup() {
  #ifdef TIMING
    Serial.begin(9600);
  #endif

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDRESS)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  // Initialize LED pins
  pinMode(RLEDPIN, OUTPUT);
  pinMode(GLEDPIN, OUTPUT);
  pinMode(BLEDPIN, OUTPUT);
  digitalWrite(RLEDPIN, LOW);
  digitalWrite(GLEDPIN, LOW);
  digitalWrite(BLEDPIN, LOW);

  // Initialize control pins
  pinMode(MINUSPIN, INPUT);
  pinMode(PLUSPIN,  INPUT);
  pinMode(MODEPIN,  INPUT);

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  delay(1000); // Pause

  // Clears the buffer, but nothing happens until display is called again
  display.clearDisplay();
  display.display();

  // Randomly draw on the screen
  // Initialize system
  display.clearDisplay();
  for (int screenX = 0; screenX < SCREEN_WIDTH; screenX+=PIXEL_WIDTH){ // screen index goes from 0 to DIM-1
    for (int screenY = 0; screenY < SCREEN_HEIGHT; screenY+=PIXEL_WIDTH){
      pixelStatus=random(0, 2);
      if (pixelStatus==1){
        draw_pixel(screenX, screenY, SSD1306_WHITE); 
      }
    }
  }
  display.display();
  delay(1000);

  // initialize RNG
  randomSeed(analogRead(UNCONNECTED_PIN));
  
}

// TO DO:
// Turn upper part of OLED into info display (T, interaction params, statistics?) (toggle with button?)
// Would be cool to benchmark how many steps we can do per sec
// Also if simulation stagnates, or after a certain time, reset

void loop() {
  #ifdef TIMING
    execTime = millis();
  #endif

  // Detect button is pressed
  // This would be easier with interrupts, but there are only 2 pins for that on the nano
  buttonState = digitalRead(PLUSPIN);
  if (buttonState==HIGH and plusState==0){
    modecounters[modecounter]+=1;
    modecounters[modecounter]=min(modecounters[modecounter], modelengths[modecounter]-1); //avoid going out of maximum array length
    plusState = 1; // avoid repeted toggles
    digitalWrite(modepins[modecounter], HIGH);
    delay(LEDDELAY);
  } else if (buttonState==LOW) {
    digitalWrite(modepins[modecounter], LOW);
    plusState = 0;
  }

  buttonState = digitalRead(MINUSPIN);
  if (buttonState==HIGH and minusState==0){
    modecounters[modecounter]-=1;
    modecounters[modecounter]=max(modecounters[modecounter], 0); //avoid going out of maximum array length
    minusState = 1; // avoid repeted toggles
    digitalWrite(modepins[modecounter], HIGH);
    delay(LEDDELAY);
  } else if (buttonState==LOW) {
    digitalWrite(modepins[modecounter], LOW);
    minusState = 0;
  }

  buttonState = digitalRead(MODEPIN);
  if (buttonState==HIGH and modeState==0){
    modeState = 1; // avoid repeted toggles
    modecounter +=1;
    if (modecounter>modemax) {
      modecounter=0;
    }
    digitalWrite(modepins[modecounter], HIGH);    
    delay(LEDDELAY);
  } else if (buttonState==LOW) {
    digitalWrite(modepins[modecounter], LOW);
    modeState = 0;
  }

  #ifdef TIMING
    Serial.print("b ");
    Serial.print(blist[modecounters[BPOS]]);
    Serial.print(" h ");
    Serial.print(hlist[modecounters[HPOS]]);
    Serial.print(" J ");
    Serial.println(Jlist[modecounters[JPOS]]);
  #endif
  
  for (int i=0; i <= CALC_STEPS; i++) { // start big for loop
    
  //Pick a pixel at random and get status
  pixelX = random(0, SCREEN_WIDTH);
  pixelX = pixelX-(pixelX%PIXEL_WIDTH); // subtract the modulo
  pixelY = random(0, SCREEN_HEIGHT);
  pixelY = pixelY-(pixelY%PIXEL_WIDTH);
  // Trial Move
  origSpin  = pixel_spin(pixelX, pixelY);
  trialSpin = -1.0*origSpin; // try opposite spin

  // one can get a direct reference to the buffer from the library and operate on it, but it is ugly, easier to go thru the functions
  //left
  spinLeft  = pixel_spin( (pixelX-PIXEL_WIDTH>0)?(pixelX-PIXEL_WIDTH):(SCREEN_WIDTH-PIXEL_WIDTH) , pixelY); //using ternary operator, harder to read but allows for single line
  //right
  spinRight = pixel_spin( (pixelX+PIXEL_WIDTH<SCREEN_WIDTH)?(pixelX+PIXEL_WIDTH):(0) , pixelY);
  //up
  spinUp    = pixel_spin(  pixelX, (pixelY-PIXEL_WIDTH>0)?(pixelY-PIXEL_WIDTH):(SCREEN_HEIGHT-PIXEL_WIDTH));
  //down
  spinDown  = pixel_spin(  pixelX, (pixelY+PIXEL_WIDTH<SCREEN_HEIGHT)?(pixelY+PIXEL_WIDTH):(0));

  dE = 2.0 * origSpin *(Jlist[modecounters[JPOS]]*(spinLeft+spinRight+spinUp+spinDown)+hlist[modecounters[HPOS]]);

  ran = 1.0*random(0, MAX_RAND+1) / MAX_RAND;
  metropolis=exp(-(dE)*blist[modecounters[BPOS]]);
  if (ran<=metropolis){ // actually flip
    if (origSpin>0.0) { // was on, turn it off
      draw_pixel(pixelX, pixelY, SSD1306_BLACK); 
    } else { // was off, turn it on
      draw_pixel(pixelX, pixelY, SSD1306_WHITE); 
    }
  } else {
    // do nothing
  }

  } // end for loop and update screen
  
  display.display();

  #ifdef TIMING
    execTime = millis() - execTime;
    Serial.print(CALC_STEPS); Serial.print(" steps in "); Serial.print(execTime); Serial.print(" ms ");
    Serial.print(float(execTime)/CALC_STEPS); Serial.println(" ms/step");
  #endif
  
}
